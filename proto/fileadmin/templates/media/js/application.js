$(document).ready(function () {

//aktiviere den Bootstrap Slider
    $('.carousel').carousel();

    $(".dropdown").hover(
        function () {
            $(this).children('.dropdown-menu').addClass("show");

        },
        function () {
            $(this).children('.dropdown-menu').removeClass("show");
        }
    );


    $('.slick-carousel').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });




    //Navbar Scroll
    $("navscroll").hide();
    $("dropbox").hide();


    var nav = document.getElementById('nav');
    var navbrand = document.getElementById('nav-brand');
    navbrand.style.display = "none";

    window.onscroll = function () {
        if (window.pageYOffset > 100) {
            nav.style.background = "#007bff";
            navbrand.style.display = "block";
        } else {
            nav.style.background = "transparent";
            navbrand.style.display = "none";
        }
    };


    // Cookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + "; ";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    var testcookie = getCookie("cookieChecked");
    if (testcookie != "") {
        $('.cookie').hide();
    } else {
        $('.cookie').removeClass('cookie-hidden');
        $('.js-close-cookie').on("click", function (e) {
            e.preventDefault();
            $('.cookie').hide();
            $('.cookie').addClass('cookie-hidden');
            setCookie("cookieChecked", "true");
        });
    }

});


am4core.ready(function () {

    //VARIABLEN NAME:
    //polygonSeries  =  worldSeries
    //polygonTemplate = worldPolygon

// Themes begin
    am4core.useTheme(am4themes_kelly);
    am4core.useTheme(am4themes_animated);
// Themes end


    var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

    var continents = {
        "AF": 0,
        "AN": 1,
        "AS": 2,
        "EU": 3,
        "NA": 4,
        "OC": 5,
        "SA": 6
    }

// Create map instance
    var chart = am4core.create("chartdiv", am4maps.MapChart);
    chart.projection = new am4maps.projections.Miller();

// Create map polygon series for world map
    var worldSeries = chart.series.push(new am4maps.MapPolygonSeries());
    worldSeries.useGeodata = true;
    worldSeries.geodata = am4geodata_worldLow;
    worldSeries.exclude = ["AQ"];






    var worldPolygon = worldSeries.mapPolygons.template;
/*    worldPolygon.tooltipText = "{name}";*/
    worldPolygon.nonScalingStroke = true;
    worldPolygon.strokeOpacity = 0.5;
    worldPolygon.fill = am4core.color("#ddd");
/*    worldPolygon.propertyFields.fill = "color";*/

    var hs = worldPolygon.states.create("hover");
    hs.properties.fill = chart.colors.getIndex(9);

    // create capital markers
    var imageSeries = chart.series.push(new am4maps.MapImageSeries());

// define template
    var imageSeriesTemplate = imageSeries.mapImages.template;
    var circle = imageSeriesTemplate.createChild(am4core.Image);
    circle.href  = "fileadmin/templates/media/images/test/marker.svg";
    circle.width = 22;
    circle.height = 22;
    circle.nonScaling = false;
    circle.tooltipText = "{title}";
    circle.horizontalCenter = "middle";
    circle.verticalCenter = "top";

// what about scale...

// set propertyfields
    imageSeriesTemplate.propertyFields.latitude = "latitude";
    imageSeriesTemplate.propertyFields.longitude = "longitude";

    imageSeriesTemplate.horizontalCenter = "middle";
    imageSeriesTemplate.verticalCenter = "middle";
    imageSeriesTemplate.align = "center";
    imageSeriesTemplate.valign = "middle";
    imageSeriesTemplate.width = 8;
    imageSeriesTemplate.height = 8;
    imageSeriesTemplate.nonScaling = true;
    imageSeriesTemplate.tooltipText = "{title}";
    imageSeriesTemplate.fill = am4core.color("#0067a5");
    imageSeriesTemplate.background.fillOpacity = 0;
    imageSeriesTemplate.background.fill = am4core.color("#ffffff");
    imageSeriesTemplate.setStateOnChildren = true;
    imageSeriesTemplate.states.create("hover");


    imageSeries.data = [{
        "title": "Vienna",
        "latitude": 48.2092,
        "longitude": 16.3728,
    }, {
        "title": "Minsk",
        "latitude": 53.9678,
        "longitude": 27.5766
    }, {
        "title": "Brussels",
        "latitude": 50.8371,
        "longitude": 4.3676
    }, {
        "title": "Sarajevo",
        "latitude": 43.8608,
        "longitude": 18.4214
    }, {
        "title": "Sofia",
        "latitude": 42.7105,
        "longitude": 23.3238
    }, {
        "title": "Zagreb",
        "latitude": 45.815,
        "longitude": 15.9785
    }, {
        "title": "Pristina",
        "latitude": 42.666667,
        "longitude": 21.166667
    }, {
        "title": "Prague",
        "latitude": 50.0878,
        "longitude": 14.4205
    }, {
        "title": "Copenhagen",
        "latitude": 55.6763,
        "longitude": 12.5681
    }, {
        "title": "Tallinn",
        "latitude": 59.4389,
        "longitude": 24.7545
    }, {
        "title": "Helsinki",
        "latitude": 60.1699,
        "longitude": 24.9384
    }, {
        "title": "Paris",
        "latitude": 48.8567,
        "longitude": 2.351
    }, {
        "title": "Berlin",
        "latitude": 52.5235,
        "longitude": 13.4115
    }, {
        "title": "Athens",
        "latitude": 37.9792,
        "longitude": 23.7166
    }];




// Create country specific series (but hide it for now)
    var countrySeries = chart.series.push(new am4maps.MapPolygonSeries());
    countrySeries.useGeodata = true;
    countrySeries.hide();
    countrySeries.geodataSource.events.on("done", function (ev) {
        worldSeries.hide();
        countrySeries.show();
    });


   /* var countryPolygon = countrySeries.mapPolygons.template;
    countryPolygon.tooltipText = "{name}";
    countryPolygon.nonScalingStroke = true;
    countryPolygon.strokeOpacity = 0.5;
    countryPolygon.fill = am4core.color("#eee");

    var hs = countryPolygon.states.create("hover");
    hs.properties.fill = chart.colors.getIndex(9);*/

// Set up click events
    worldPolygon.events.on("hit", function (ev) {
        ev.target.series.chart.zoomToMapObject(ev.target);
        var map = ev.target.dataItem.dataContext.map;
        if (map) {
            ev.target.isHover = false;
            countrySeries.geodataSource.url = "https://www.amcharts.com/lib/4/geodata/json/" + map + ".json";
            countrySeries.geodataSource.load();
        }
    });

// Set up data for countries
/*    var data = [];
    for (var id in am4geodata_data_countries2) {
        if (am4geodata_data_countries2.hasOwnProperty(id)) {
            var country = am4geodata_data_countries2[id];
            if (country.maps.length) {
                data.push({
                    id: id,
                    color: chart.colors.getIndex(continents[country.continent_code]),
                    map: country.maps[0]
                });
            }
        }
    }
    worldSeries.data = data;*/

// Zoom control

    chart.zoomControl = new am4maps.ZoomControl();
    chart.zoomControl.plusButton.background.fill = am4core.color("#0067a5");
    chart.zoomControl.plusButton.fill = am4core.color("#fff");
    chart.zoomControl.plusButton.cursorOverStyle = am4core.MouseCursorStyle.pointer;

    chart.zoomControl.minusButton.background.fill = am4core.color("#0067a5");
    chart.zoomControl.minusButton.fill = am4core.color("#fff");
    chart.zoomControl.minusButton.cursorOverStyle = am4core.MouseCursorStyle.pointer;

    var homeButton = new am4core.Button();
    homeButton.events.on("hit", function () {
        worldSeries.show();
        countrySeries.hide();
        chart.goHome();
    });

    homeButton.icon = new am4core.Sprite();
    homeButton.padding(7, 5, 7, 5);
    homeButton.width = 30;
    homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
    homeButton.marginBottom = 10;
    homeButton.parent = chart.zoomControl;
    homeButton.cursorOverStyle = am4core.MouseCursorStyle.pointer;
    homeButton.fill = am4core.color("#fff");
    homeButton.background.fill = am4core.color("#0067a5");
    homeButton.insertBefore(chart.zoomControl.plusButton);

    chart.homeZoomLevel = 3.5;
    chart.homeGeoPoint = {longitude: 10, latitude: 52};

}); // end am4core.ready()


// Initialize and add the map
function initMap() {
    // The location of Uluru
    var wolnzach = {lat: 48.607864, lng: 11.601322};
    // The map, centered at Uluru
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 12, center: wolnzach});

    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({
        position: wolnzach,
        map: map,
        title: 'Demmar GmbH'
    });
}