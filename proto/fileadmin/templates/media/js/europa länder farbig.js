$(document).ready(function () {

//aktiviere den Bootstrap Slider
    $('.carousel').carousel();

    $(".dropdown").hover(
        function () {
            $(this).children('.dropdown-menu').addClass("show");

        },
        function () {
            $(this).children('.dropdown-menu').removeClass("show");
        }
    );


    $('.slick-carousel').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });


    //Navbar Scroll
    $("navscroll").hide();
    $("dropbox").hide();


    var nav = document.getElementById('nav');
    var navbrand = document.getElementById('nav-brand');
    navbrand.style.display = "none";

    window.onscroll = function () {
        if (window.pageYOffset > 100) {
            nav.style.background = "#007bff";
            navbrand.style.display = "block";
        } else {
            nav.style.background = "transparent";
            navbrand.style.display = "none";
        }
    };


    // Cookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + "; ";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    var testcookie = getCookie("cookieChecked");
    if (testcookie != "") {
        $('.cookie').hide();
    } else {
        $('.cookie').removeClass('cookie-hidden');
        $('.js-close-cookie').on("click", function (e) {
            e.preventDefault();
            $('.cookie').hide();
            $('.cookie').addClass('cookie-hidden');
            setCookie("cookieChecked", "true");
        });
    }

});
am4core.ready(function () {

// Themes begin
    am4core.useTheme(am4themes_animated);
// Themes end



    // Create map instance
    var chart = am4core.create("chartdiv", am4maps.MapChart);
    // Set projection
    chart.projection = new am4maps.projections.Miller();


// Set map definition
    chart.geodata = am4geodata_worldLow;

// Zoom control
    chart.zoomControl = new am4maps.ZoomControl();

    var homeButton = new am4core.Button();
    homeButton.events.on("hit", function () {
        chart.goHome();
    });

    homeButton.icon = new am4core.Sprite();
    homeButton.padding(7, 5, 7, 5);
    homeButton.width = 30;
    homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
    homeButton.marginBottom = 10;
    homeButton.parent = chart.zoomControl;
    homeButton.insertBefore(chart.zoomControl.plusButton);

// Center on the groups by default
    chart.homeZoomLevel = 3.5;
    chart.homeGeoPoint = {longitude: 10, latitude: 52};




// This array will be populated with country IDs to exclude from the world series
    var excludedCountries = ["AQ"];

// Create a series for each group, and populate the above array
    groupData.forEach(function (group) {
        var series = chart.series.push(new am4maps.MapPolygonSeries());
        series.name = group.name;
        series.useGeodata = true;
        var includedCountries = [];
        group.data.forEach(function (country) {
            includedCountries.push(country.id);
            excludedCountries.push(country.id);
        });
        series.include = includedCountries;

        series.fill = am4core.color(group.color);

        // By creating a hover state and setting setStateOnChildren to true, when we
        // hover over the series itself, it will trigger the hover SpriteState of all
        // its countries (provided those countries have a hover SpriteState, too!).
        series.setStateOnChildren = true;
        series.calculateVisualCenter = true;


        // Country shape properties & behaviors
        var mapPolygonTemplate = series.mapPolygons.template;
        // Instead of our custom title, we could also use {name} which comes from geodata
        mapPolygonTemplate.fill = am4core.color(group.color);
        mapPolygonTemplate.fillOpacity = 0.8;
        mapPolygonTemplate.nonScalingStroke = true;
        mapPolygonTemplate.tooltipPosition = "fixed"

        mapPolygonTemplate.events.on("over", function (event) {
            series.mapPolygons.each(function (mapPolygon) {
                mapPolygon.isHover = true;
            })
            event.target.isHover = false;
            event.target.isHover = true;
        })

        mapPolygonTemplate.events.on("out", function (event) {
            series.mapPolygons.each(function (mapPolygon) {
                mapPolygon.isHover = false;
            })
        })


        // States
        var hoverState = mapPolygonTemplate.states.create("hover");
        hoverState.properties.fill = am4core.color("#0511F2");

        // Tooltip
        mapPolygonTemplate.tooltipText = "{title}"; // enables tooltip
        // series.tooltip.getFillFromObject = false; // prevents default colorization, which would make all tooltips red on hover
        // series.tooltip.background.fill = am4core.color(group.color);

        // MapPolygonSeries will mutate the data assigned to it,
        // we make and provide a copy of the original data array to leave it untouched.
        // (This method of copying works only for simple objects, e.g. it will not work
        //  as predictably for deep copying custom Classes.)
        series.data = JSON.parse(JSON.stringify(group.data));
    });

// The rest of the world.
    var worldSeries = chart.series.push(new am4maps.MapPolygonSeries());
    var worldSeriesName = "world";
    worldSeries.name = worldSeriesName;
    worldSeries.useGeodata = true;
    worldSeries.exclude = excludedCountries;
    worldSeries.fillOpacity = 0.8;
    worldSeries.hiddenInLegend = true;
    worldSeries.mapPolygons.template.nonScalingStroke = true;



// create capital markers
    var imageSeries = chart.series.push(new am4maps.MapImageSeries());

    var hs = worldPolygon.states.create("hover");
    hs.properties.fill = am4core.color("#0511F2");



// define template
    var imageSeriesTemplate = imageSeries.mapImages.template;
    var circle = imageSeriesTemplate.createChild(am4core.Sprite);
    circle.scale = 0.4;
    circle.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
    circle.path = targetSVG;
// what about scale...

// set propertyfields  ***Städte setzen***
    imageSeriesTemplate.propertyFields.latitude = "latitude";
    imageSeriesTemplate.propertyFields.longitude = "longitude";

    imageSeriesTemplate.horizontalCenter = "middle";
    imageSeriesTemplate.verticalCenter = "middle";
    imageSeriesTemplate.align = "center";
    imageSeriesTemplate.valign = "middle";
    imageSeriesTemplate.width = 8;
    imageSeriesTemplate.height = 8;
    imageSeriesTemplate.nonScaling = true;
    imageSeriesTemplate.tooltipText = "{title}";
    imageSeriesTemplate.fill = am4core.color("#000");
    imageSeriesTemplate.background.fillOpacity = 0;
    imageSeriesTemplate.background.fill = am4core.color("#ffffff");
    imageSeriesTemplate.setStateOnChildren = true;
    imageSeriesTemplate.states.create("hover");

    imageSeries.data = [{
        "title": "Vienna",
        "latitude": 48.2092,
        "longitude": 16.3728
    }, {
        "title": "Minsk",
        "latitude": 53.9678,
        "longitude": 27.5766
    }, {
        "title": "Brussels",
        "latitude": 50.8371,
        "longitude": 4.3676
    }, {
        "title": "Sarajevo",
        "latitude": 43.8608,
        "longitude": 18.4214
    }, {
        "title": "Sofia",
        "latitude": 42.7105,
        "longitude": 23.3238
    }, {
        "title": "Zagreb",
        "latitude": 45.815,
        "longitude": 15.9785
    }, {
        "title": "Pristina",
        "latitude": 42.666667,
        "longitude": 21.166667
    }, {
        "title": "Prague",
        "latitude": 50.0878,
        "longitude": 14.4205
    }, {
        "title": "Copenhagen",
        "latitude": 55.6763,
        "longitude": 12.5681
    }];

    //***Städte setzen ENDE***

// This auto-generates a legend according to each series' name and fill
    /*    chart.legend = new am4maps.Legend();*/

// Legend styles
    /*    chart.legend.paddingLeft = 27;
        chart.legend.paddingRight = 27;
        chart.legend.marginBottom = 15;
        chart.legend.width = am4core.percent(90);
        chart.legend.valign = "bottom";
        chart.legend.contentAlign = "left";*/

// Legend items
    /*    chart.legend.itemContainers.template.interactionsEnabled = false;*/

}); // end am4core.ready()